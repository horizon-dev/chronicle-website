FROM python:3.6.5-stretch
ARG USER
ARG GROUP
ARG UID
ARG GID
COPY requirements.txt /website/
WORKDIR /website
RUN pip install -r requirements.txt
RUN git clone https://github.com/fle/pelican-simplegrey.git /theme
COPY theme.patch /theme
RUN cd /theme && patch -p1 < theme.patch
RUN git clone https://github.com/neverpanic/google-font-download.git /gfd \
    && mkdir -p /theme/static/fonts \
    && cd /theme/static/fonts \
    && /gfd/google-font-download --url="http://fonts.googleapis.com/css?\
        family=Source+Sans+Pro:300,400,600,300italic,400italic,600italic\
        |Source+Code+Pro"
COPY . /website
RUN groupadd -fg $GID $GROUP \
    && useradd -ms /bin/bash -u $UID -g $GROUP $USER \
    && chown -R $USER:$GROUP /website
USER $USER:$GROUP
