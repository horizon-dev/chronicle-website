#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Horizon Digital Economy Research'
SITENAME = 'Chronicle'
SITEURL = os.getenv('SITEURL', default='')

THEME = '/theme'

PATH = 'content'

INDEX_SAVE_AS = 'blog.html'

DISPLAY_PAGES_ON_MENU = False

TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

DEFAULT_PAGINATION = 10

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

LINKS = (
    ('Timeline Server Documentation',
        'https://chronicle.horizon.ac.uk/timeline-server/timeline-server.html'),
    ('Chronicle on Bitbucket',
        'https://bitbucket.org/account/user/horizon-dev/projects/CHRON'),
    ('Horizon Services Campaign', 'https://services.wp.horizon.ac.uk'),
    ('Horizon', 'https://www.horizon.ac.uk/'),
)

SOCIAL = None
