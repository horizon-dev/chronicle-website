Title: Terms &amp; Conditions
save_as: terms.html

The Chronicle platform, website, and related service campaign project services
and websites are owned and operated by [Horizon Digital Economy Research][horizon]
at the [University of Nottingham][nottingham]. The following terms, conditions,
and policies apply to the aforementioned platforms, services, websites.

* Terms &amp; Conditions: <https://www.nottingham.ac.uk/utilities/terms.aspx>
* Privacy Policy: <https://www.nottingham.ac.uk/utilities/privacy/privacy.aspx>
* Cookie Policy: <https://www.nottingham.ac.uk/utilities/cookies.aspx>

[horizon]: https://www.horizon.ac.uk
[nottingham]: https://www.nottingham.ac.uk
