Title: Chronicle
save_as: index.html

Welcome to the Chronicle website. Chronicle is a work-in-progress platform
designed to support research projects at [Horizon Digital Economy
Research][horizon] under the [services campaign][services] umbrella.

The key component of the platform and the first in development is the
**Timeline Server**, you can read the work-in-progress documentation at
[timeline-server/timeline-server.html][timeline-server].

You can find an introduction to Chronicle on the
[services campaign blog][chronicle-blog]

[horizon]: https://www.horizon.ac.uk
[services]: https://services.wp.horizon.ac.uk
[timeline-server]: https://chronicle.horizon.ac.uk/timeline-server/timeline-server.html
[chronicle-blog]: https://services.wp.horizon.ac.uk/2018/03/07/chronicling-the-lives-of-objects/
