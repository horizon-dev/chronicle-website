import logging
import os
import re
import sys

import asyncio
import asyncssh


__log_level = 'DEBUG'

__config = {
    'publish_server_hostname': os.getenv('PUBLISH_SERVER_HOSTNAME'),
    'publish_server_username': os.getenv('PUBLISH_SERVER_USERNAME'),
    'publish_dir': os.getenv('PUBLISH_DIR'),
    'site_dir': '../output/',
}

logging.basicConfig(level=__log_level)
__logger = logging.getLogger(__name__)


async def main(config):
    global __logger
    async with asyncssh.connect(
        config['publish_server_hostname'],
        username=config['publish_server_username'],
        client_keys=[config['identity_file']]
    ) as conn:
        for f in os.listdir(config['site_dir']):
            await asyncssh.scp(
                config['site_dir'] + '/' + f,
                (conn, config['publish_dir']),
                recurse=True)


if __name__ == '__main__':
    try:
        with open(os.path.join(os.getenv("HOME"), '.ssh/config')) as f:
            rx = re.compile(r'IdentityFile\s*(.*)')
            lines = f.readlines()
            for line in lines:
                m = rx.match(line)
                if m:
                    __config['identity_file'] = m.group(1)
                    break
        asyncio.get_event_loop().run_until_complete(main(__config))
    except (OSError, asyncssh.Error) as exc:
        sys.exit('SSH connection failed: ' + str(exc))
